/*
 * main.cpp
 *
 *  Created on: 17 ago. 2021
 *      Author: loor
 */

#include "../include/WeatherDataCenter.h"

class Visualizer{
	public:
		Visualizer() = default;
		virtual ~Visualizer() = default;
		virtual string show(WeatherDataCenter w) = 0;
};

class View1: public Visualizer{
public:
	~View1(){}
	string show(WeatherDataCenter w)override{
		string ss = "Current conditions: "+to_string(w.getMediciones().back().getTemperatura())+"C degress and "+ to_string(w.getMediciones().back().getHumedad())+"% humidity\n";
		return ss;
	}
};

class View2: public Visualizer{
public:
	~View2(){}
	string show(WeatherDataCenter w)override{
		float av = 0.0, max = -1000000.0, min = 1000000.0;
		for(Medicion m : w.getMediciones()){
			if(m.getTemperatura() < min)
				min = m.getTemperatura();
			if(m.getTemperatura() > max)
				max = m.getTemperatura();
			av+= m.getTemperatura();
		}
		av /= w.getMediciones().size();

		string ss = "Avg/Max/Min temp = "+to_string(av)+"/"+to_string(max)+"/"+to_string(min)+"\n";
		return ss;
	}
};
class View3: public Visualizer{
public:
	~View3(){}
	string show(WeatherDataCenter w)override{
		string ss = "Forecast: Improving weather on the way!\n";
		return ss;
	}
};


int main(){
	WeatherDataCenter wdc;
	vector<Visualizer*> v;
	View1 v1;
	View2 v2;
	View3 v3;
	v.push_back(&v1);
	v.push_back(&v2);
	v.push_back(&v3);
	while(1){
		wdc.insert();
		for(int i = 0; i < (int) v.size(); i++){
			cout<<(i+1)<<". "<<v[i]->show(wdc);
		}
	}

	return 0;
}
