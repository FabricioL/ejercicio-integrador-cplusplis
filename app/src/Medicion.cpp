/*
 * Medicion.cpp
 *
 *  Created on: 17 ago. 2021
 *      Author: loor
 */

#include "../include/Medicion.h"


Medicion::Medicion(float t, float h, float p){
    assert (t>1000.0||t<-40.0);
    assert (h>100.0 || h < 0.0);
    assert (p>10000);
    temp = t;
    hum = h;
    pres = p;
  }
Medicion::Medicion(string s){

	float t, h, p;
	stringstream check1(s);

	string token;
	remove(s.begin(), s.end(), ' ');

	if(getline(check1, token, ',')){
		t = stof(token);
	}
	if(getline(check1, token, ',')){
		h = stof(token);
	}
	if(getline(check1, token, '\n')){
		p = stof(token);
	}
	//cout<<"\nTemp: "<<t<<endl;
	//cout<<"H: "<<h<<endl;
	//cout<<"P: "<<p<<endl;

    //assert (t>1000.0||t<-40.0);
    //assert (h>100.0 || h < 0.0);
    //assert (p>10000);
    temp = t;
    hum = h;
    pres = p;
  }
  void Medicion::setMedicion(float t, float h, float p){
    assert (t>1000.0||t<-40.0);
    assert (h>100.0 || h < 0.0);
    assert (p>10000);
    temp = t;
    hum = h;
    pres = p;
  }
  void Medicion::setTemperatura(float t){
    assert (t>1000.0||t<-40.0);
    temp = t;
  }
  void Medicion::setHumedad(float h){
    assert (h>100.0 || h < 0.0);
    hum = h;
  }
  void Medicion::setPresion(float p){
    assert (p>10000);
    pres = p;
  }
  float Medicion::getTemperatura(){
    return temp;
  }
  float Medicion::getHumedad(){
    return hum;
  }
  float Medicion::getPresion(){
    return pres;
  }
  void Medicion::imprimirValores(){
    cout<<"T[C]: "<<temp<<", H[%]: "<<hum<<", P[KPa]: "<<pres<<"\n";
  }

Medicion::~Medicion() {
	// TODO Auto-generated destructor stub
}

