/*
 * Medicion.h
 *
 *  Created on: 17 ago. 2021
 *      Author: loor
 */

#ifndef SRC_MEDICION_H_
#define SRC_MEDICION_H_
#include <assert.h>
#include <bits/stdc++.h>

using namespace std;

class Medicion {
private:
	float temp;
	float hum;
	float pres;
public:
	Medicion(float t, float h, float p);
	Medicion(string);
	virtual ~Medicion();
	void setMedicion(float t, float h, float p);
	void setTemperatura(float t);
	void setHumedad(float h);
	void setPresion(float p);
	float getTemperatura();
	float getHumedad();
	float getPresion();
	void imprimirValores();
};

#endif /* SRC_MEDICION_H_ */
