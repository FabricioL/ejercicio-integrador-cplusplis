/*
 * WeatherDataCenter.h
 *
 *  Created on: 17 ago. 2021
 *      Author: loor
 */

#ifndef SRC_WEATHERDATACENTER_H_
#define SRC_WEATHERDATACENTER_H_
#include <vector>
#include <iostream>
#include "Medicion.h"
#include <sstream>

using namespace std;

class WeatherDataCenter {
private:
	vector<Medicion> vs;
public:
	WeatherDataCenter();
	virtual ~WeatherDataCenter();
	void insert();
	vector<Medicion> getMediciones();
};

#endif /* SRC_WEATHERDATACENTER_H_ */
